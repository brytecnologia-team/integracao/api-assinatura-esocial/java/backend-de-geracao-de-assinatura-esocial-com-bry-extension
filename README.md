# API de Geração de Assinatura eSocial com BRy Extension - Backend

Este é um exemplo backend de integração dos serviços da API de assinatura com clientes baseados em tecnologia Java.

Este exemplo é integrado com o [frontend](https://gitlab.com/brytecnologia-team/integracao/api-assinatura-esocial/react/geracao-de-assinatura-esocial-com-bry-extension) de geração de assinatura. 

A aplicação realiza comunicação com o serviço de assinatura do HubFW.

### Tech

O exemplo utiliza a biblioteca Java abaixo:
* [JDK 8] - Java 8
* [Spring-boot] - Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run".

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência (utilizamos o Eclipse).

Executar programa:

	clique com o botão direito em cima do arquivo SignatureApplication.java -> Run as -> Java Application

   [JDK 8]: <https://www.oracle.com/java/technologies/javase-jdk8-downloads.html>
   [Spring-boot]: <https://spring.io/projects/spring-boot>
   [frontend]: <https://gitlab.com/brytecnologia-team/integracao/api-assinatura/javascript/frontend-assinatura-xml-com-bry-extension>
   