package br.com.bry.hub.exemplo.models;

import java.math.BigInteger;

public class InitializedDocument {

	protected BigInteger nonce;

	protected String content;

	public InitializedDocument() {

	}

	public InitializedDocument(BigInteger nonce, String content) {
		setNonce(nonce);
		setContent(content);
	}

	public void setNonce(BigInteger nonce) {
		this.nonce = nonce;
	}

	public void setContent(String conteudo) {
		this.content = conteudo;
	}

	public BigInteger getNonce() {
		return nonce;
	}

	public String getContent() {
		return content;
	}

	@Override
	public String toString() {
		return "InitializedDocument [" + "nonce=" + nonce + ", content=" + content + "]";
	}
}