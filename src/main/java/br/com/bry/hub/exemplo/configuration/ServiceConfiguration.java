package br.com.bry.hub.exemplo.configuration;

import java.io.File;

public class ServiceConfiguration {

	private static final String PATH_SEPARATOR = File.separator;
	
	public static final String SERVICE_BASE_URL = "https://hub2.hom.bry.com.br";
	//public static final String SERVICE_BASE_URL = "https://hub2.bry.com.br";
	
	public static final String INITIALIZE_SERVICE_URL = SERVICE_BASE_URL
			+ "/api/xml-signature-service/v2/signatures/initialize";
	public static final String FINALIZE_SERVICE_URL = SERVICE_BASE_URL
			+ "/api/xml-signature-service/v2/signatures/finalize";
	
	public static final String OUTPUT_RESOURCES_FOLDER = System.getProperty("user.dir") + "" + PATH_SEPARATOR + "generatedSignatures" + PATH_SEPARATOR;
}
