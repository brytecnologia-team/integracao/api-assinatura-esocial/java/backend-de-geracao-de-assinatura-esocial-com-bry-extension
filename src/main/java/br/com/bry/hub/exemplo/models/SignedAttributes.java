package br.com.bry.hub.exemplo.models;

public class SignedAttributes {

	protected Integer nonce;

	protected String content;

	public SignedAttributes() {

	}

	public SignedAttributes(Integer nonce, String content) {
		setNonce(nonce);
		setContent(content);
	}

	public void setNonce(Integer nonce) {
		this.nonce = nonce;
	}

	public void setContent(String conteudo) {
		this.content = conteudo;
	}

	public Integer getNonce() {
		return nonce;
	}

	public String getContent() {
		return content;
	}

	public String toString() {
		return "SignedAttribute [" + "nonce=" + nonce + ", content=" + content + "]";
	}
}