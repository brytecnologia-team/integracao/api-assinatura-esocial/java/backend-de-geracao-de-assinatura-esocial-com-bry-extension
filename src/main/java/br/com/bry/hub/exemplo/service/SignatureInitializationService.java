package br.com.bry.hub.exemplo.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import br.com.bry.hub.exemplo.configuration.ServiceConfiguration;
import br.com.bry.hub.exemplo.models.InitializationResponse;

@Component
public class SignatureInitializationService {

	@Autowired
	private RestTemplate restTemplate;

	/**
	 * Performs communication with the Signature API endpoint
	 * 
	 * @param request
	 * @return Initialization response from Signature API
	 * @throws Exception
	 */
	public InitializationResponse initializeSignature(HttpServletRequest request) throws Exception {
		ResponseEntity<InitializationResponse> responseInitialize = null;

		HttpEntity<?> requestToAPI = this.getHttpEntity(request);

		responseInitialize = restTemplate.postForEntity(ServiceConfiguration.INITIALIZE_SERVICE_URL, requestToAPI,
				InitializationResponse.class);

		return responseInitialize.getBody();

	}

	/**
	 * Analyze the request received from the front-end and configure the request
	 * that will be sent to the Signature API endpoint
	 * 
	 * @param request
	 * @return Request to the Signature API endpoint
	 */
	private HttpEntity<?> getHttpEntity(HttpServletRequest request) {
		List<MultipartFile> currentDocumentStreamContentValue = ((StandardMultipartHttpServletRequest) request)
				.getMultiFileMap().get("documento");

		Resource resourceOriginalDocument = null;

		if (!currentDocumentStreamContentValue.isEmpty()) {
			resourceOriginalDocument = currentDocumentStreamContentValue.get(0).getResource();
		}
		
		String certificate = request.getParameterValues("certificate")[0];

		final HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", request.getHeader("Authorization"));
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
		
		map.add("nonce", "1");
		map.add("signatureFormat", "ENVELOPED");
		map.add("hashAlgorithm", "SHA256");
		map.add("certificate", certificate);
		map.add("profile", "BASIC");
		map.add("returnType", "BASE64");
		map.add("canonicalizerType", "INCLUSIVE");
		map.add("generateSimplifiedXMLDSig", "true");
		map.add("includeXPathEnveloped", "false");
		map.add("originalDocuments[0][nonce]", "1");
		map.add("originalDocuments[0][content]", resourceOriginalDocument);

		return new HttpEntity<>(map, headers);
	}

}
