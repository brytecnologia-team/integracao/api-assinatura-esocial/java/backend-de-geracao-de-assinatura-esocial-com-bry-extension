package br.com.bry.hub.exemplo.util;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import br.com.bry.hub.exemplo.configuration.ServiceConfiguration;

public class FileUtil {
	
	public static void writeContentToFile(String fileName, String content) throws IOException {
		
		String filePath = ServiceConfiguration.OUTPUT_RESOURCES_FOLDER;

		SimpleDateFormat formatter = new SimpleDateFormat("_yyyy-MM-dd hh.mm.ss.SSS");

		String signatureFullName = filePath + fileName + formatter.format(new Date(System.currentTimeMillis())) + ".xml";
		
		FileOutputStream in = new FileOutputStream(new File(signatureFullName));
		in.write(Base64.getDecoder().decode(content));
		in.close();

		System.out.println("Successful signature generation: " + signatureFullName);
	}
}
