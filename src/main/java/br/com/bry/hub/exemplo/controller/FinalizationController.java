package br.com.bry.hub.exemplo.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.bry.hub.exemplo.service.SignatureFinalizationService;
import br.com.bry.hub.exemplo.util.FileUtil;

@RestController
public class FinalizationController {

	@Autowired
	private SignatureFinalizationService finalizationService;

	/**
	 * Back-end finalization endpoint
	 * 
	 * @param request
	 * @param response
	 * @return Finalization response
	 * @throws Exception
	 */
	@CrossOrigin(origins = "*")
	@PostMapping(value = "/finalize", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<List<String>> finalizeSignature(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		System.out.println("Serviço de finalização inicializado.");
		
		List<String> content = finalizationService.finalizeSignature(request);

		for (int i = 0; i < content.size(); i++)
			FileUtil.writeContentToFile("signature-item-" + i, content.get(0));

		return new ResponseEntity<>(finalizationService.finalizeSignature(request), HttpStatus.OK);
	}

}
