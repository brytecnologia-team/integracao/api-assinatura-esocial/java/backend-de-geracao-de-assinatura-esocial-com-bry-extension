package br.com.bry.hub.exemplo.models;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class InitializationResponse {

	private List<SignedAttributes> signedAttributes;

	private List<InitializedDocument> initializedDocuments;

	private BigInteger nonce;

	public List<InitializedDocument> getInitializedDocuments() {
		return initializedDocuments;
	}

	public void setInitializedDocuments(List<InitializedDocument> initializedDocuments) {
		this.initializedDocuments = initializedDocuments;
	}

	public List<SignedAttributes> getSignedAttributes() {
		return signedAttributes;
	}

	public void setSignedAttributes(List<SignedAttributes> signedAttributes) {
		this.signedAttributes = signedAttributes;
	}

	public void addSignedAttribute(SignedAttributes signedAttribute) {
		if (this.signedAttributes == null) {
			this.signedAttributes = new ArrayList<SignedAttributes>();
		}
		this.signedAttributes.add(signedAttribute);
	}

	public BigInteger getNonce() {
		return nonce;
	}

	public void setNonce(BigInteger nonce) {
		this.nonce = nonce;
	}

	public String toString() {
		return "InitializationResponse [" + "nonce=" + nonce + ", signedAttributes=" + signedAttributes+ "]";
	}
}