package br.com.bry.hub.exemplo.exceptions;

import java.io.Serializable;

public class ErrorResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4962511892835538692L;
	private int status;
	private Error error;
	private long timestamp;

	public ErrorResponse(int status, Error error, long timestamp) {
		super();
		this.setStatus(status);
		this.setError(error);
		this.setTimestamp(timestamp);
	}

	private ErrorResponse(ErrorBuilder errorBuilder) {
		this.setStatus(errorBuilder.status);
		this.setTimestamp(errorBuilder.timestamp);
		this.setError(errorBuilder.error);
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public static class Error implements Serializable {

		private static final long serialVersionUID = 6213892175433951153L;

		private String key;
		private String message;

		public Error() {
			super();

		}

		public Error(String key, String message) {
			super();
			this.key = key;
			this.message = message;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}
	}

	public static class ErrorBuilder {

		private int status;
		private Error error;
		private long timestamp;

		public ErrorBuilder() {
			super();
		}

		public ErrorBuilder(int status, Error error, long timestamp) {
			super();
			this.status = status;
			this.error = error;
			this.timestamp = timestamp;
		}

		public ErrorResponse build() {
			return new ErrorResponse(this);
		}

		public ErrorBuilder status(int status) {
			this.status = status;
			return this;
		}

		public ErrorBuilder error(Error error) {
			this.error = error;
			return this;
		}

		public ErrorBuilder timestamp(long timestamp) {
			this.timestamp = timestamp;
			return this;
		}
	}

}
