package br.com.bry.hub.exemplo.models;

import java.util.List;

public class Request {

	private String nonce;
	private String signatureFormat;
	private String hashAlgorithm;
	private String certificate;
	private String profile;
	private String returnType;
	private String canonicalizerType;
	private String generateSimplifiedXMLDSig;
	private String includeXPathEnveloped;
	private List<String> documentNonces;

	public String getNonce() {
		return nonce;
	}

	public void setNonce(String nonce) {
		this.nonce = nonce;
	}
	
	public String getSignatureFormat() {
		return signatureFormat;
	}

	public void setSignatureFormat(String signatureFormat) {
		this.signatureFormat = signatureFormat;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}
	
	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}

	public String getCanonicalizerType() {
		return canonicalizerType;
	}

	public void setCanonicalizerType(String canonicalizerType) {
		this.canonicalizerType = canonicalizerType;
	}

	public String getGenerateSimplifiedXMLDSig() {
		return generateSimplifiedXMLDSig;
	}

	public void setGenerateSimplifiedXMLDSig(String generateSimplifiedXMLDSig) {
		this.generateSimplifiedXMLDSig = generateSimplifiedXMLDSig;
	}

	public String getIncludeXPathEnveloped() {
		return includeXPathEnveloped;
	}

	public void setIncludeXPathEnveloped(String includeXPathEnveloped) {
		this.includeXPathEnveloped = includeXPathEnveloped;
	}

	public String getHashAlgorithm() {
		return hashAlgorithm;
	}

	public void setHashAlgorithm(String hashAlgorithm) {
		this.hashAlgorithm = hashAlgorithm;
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	public List<String> getDocumentNonce() {
		return documentNonces;
	}

	public void setDocumentNonce(List<String> documentNonces) {
		this.documentNonces = documentNonces;
	}

}
